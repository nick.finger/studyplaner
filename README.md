![studyplaner.png](/readme_images/studyplaner.gif)

# StudyPlaner

A simple tool implemented through a single html file, which can be embedded in the Content Managment System of the TU Darmstadt (FirstSpirit) an therefore be built into the websites. The tool can be used to reorganize and expand the study courses.

## Table of Contents

- [StudyPlaner](#studyplaner)
  - [Table of Contents](#table-of-contents)
  - [How to use/modify](#how-to-usemodify)
    - [Change Data for mandatory courses](#change-data-for-mandatory-courses)
    - [Change Data for Elective courses](#change-data-for-elective-courses)
    - [Change creditpoint requirements](#change-creditpoint-requirements)
    - [Change text of labes etc.](#change-text-of-labes-etc)
  - [Dependencies](#dependencies)
  - [TODO](#todo)
  - [Not-TODO](#not-todo)

## How to use/modify

Clone the repository, modify the file to your wishes and embed it, or use it locally.

### Change Data for mandatory courses
Mandatory courses are the courses, that appear from the beginning in the planer. These are stored in a JSON-Array with the format shown in the code below
```javascript
[
    {
        // Semester 1
        id: 1,
        items: [
            {
                // Course 1 of semester 1
                id: 11,
                class: "modul <type> <cycle>",
                text: "Materialwissenschaft 1",
                creditpoints: 4
            },
        ...
    },
    ...
]
```
Here `id` is the id of the column which is among other things used to determine the cylce of each column (semester). `items` holds the courses of the semester. Here `id` is a unique id to identifiy the course, `class` determines the look and behaviour of the course in the planer:
- `modul` is standard and should not be removed
- `<type>` is the type of the course the options here are:
    - `vorlesung` - for lectures/excercises/seminars which displays the course in blue 
      - ![lecture.png](readme_images/lecture.png)
    - `praktikum` - for practical lab courses or internships 
      - ![labCourse.png](readme_images/labCourse.png)
    - `nebenfach` - for elective courses (both technical and non-technical) 
      - ![elective.png](readme_images/electiveCourse.png)
    - `thesis` - for thesis (also increases the height of the module) 
      - ![thesis.png](readme_images/thesis.png)
  
- `<cycle>` determines the semester (winter or summer) in which the course is offered.
    - `wise` for winter semesters
    - `sose` for summer semesters
    - `wise sose` for both
    
The property, `text` is the display name of the course and `creditpoints` the number of creditpoints that the student gets when passing the course. 

### Change Data for Elective courses
The data is stored JSON-Arrays as shown in the code snippet below. I'd recommend collecting the data in a spreadsheet an then converting this data to a JSON array using e.g [this tool](https://tableconvert.com/excel-to-json). This also allows the JSON array to be minified.
```javascript
/**
 * Hardcoded Data for Elective Courses Datalists
 * First element is technical elective courses
 * Second element is non-technical elective courses
 */
function nebenfachData() {
		return {
			techElective: [{ "moduleNumber": "11-01-3029", "name": "Advanced Light Microscopy", "creditpoints": 4, "cycle": "sose" }, ...],
			nonTechElective: [{ "moduleNumber": "03-04-1037", "name": "Biomechanik", "creditpoints": 3, "cycle": "sose wise" }, ...]
		}
	}
```

Please note that each course must have all 4 properties (`moduleNumber`,`name`,`creditpoints` and `cycle`). If the cycle is not known put `"sose wise"`. So it could be added to both cylces, but must be controlled by the user. 

### Change creditpoint requirements

The CP-counter is part of the `footer`. Both are objects, which get initialized at the end of the javascript with the following statement:
```javascript
    const footer = new TableFooter(document.body);
```
So the change the property `requirements` of the `CPCounter`-Object add a line below the instantiation of the `TableFooter` like so:
```javascript
    const footer = new TableFooter(document.body);
    // Add the following line
    footer.counter.requirements = { total: 120, technical: 29, nonTechnical: 9 };
```

### Change text of labes etc.

My goal for this planer was and still is to make it really easy to migrate it to another plan of study or to translate the whole thing. For this reason all the text for labels and buttons and so on are stored in yet another JSON-Object so they're all at one place:
```javascript
function translationData() {
		return {
			//General things in the document
			document: {
				title: "Study Planer M.Sc."
			},
			activateScreen: {
				standard: "Click anywhere to begin",
				mobile: "This tool was built for desktop browsers. It has no mobile support. Click anywhere to begin anyway"
			},
			...
	}
}
```

## Dependencies

While this tool uses the [dragula](https://github.com/bevacqua/dragula) library (MIT License) for the drag and drop functionality, as well as the [dom-to-image](https://github.com/tsayen/dom-to-image) library (MIT License)  to convert the planer to an svg or png, there are no dependencies as both these libraries are embedded into the code. 

## TODO
- Clean up the code
- Comment the code properly
- Implement feature to export and import data for switching between devices
- maybe: adjust tool to also work on mobile devices 

## Not-TODO
- Implement anything that requires a server or any similar infrastructure. This includes
    - public pool of elective courses or something similar
The tool is supposed to require minimal maintenance.

